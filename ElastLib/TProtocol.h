class TProtocolCManager{
      variant  Clients;
      int      bThread;
      int      hSem;
}
class TProtoConnection{
public:
      TCrypto           Crypto;
      TPack             Pack;       
      TProtoConnection  Prev;
      TProtoConnection  Next;
      variant           Apps;          	//здесь содержится TProtocol::AppItem
      variant           ClntAppToSrv;  //the sequence number is the client app index, contains the the server app index
      variant           SrvAppToClnt;  //the sequence number is the server app index, contains the the client app index
      variant           AppCustomDataT; //the app custom data that the app need to attach for every unique connection
      BYTE              *InitialBufData; //данные, которые клиент сервера присылает в начале, например, session key для идентификации
      int               InitialBufSize;
}
class TProtoSockData{
      int               ClntAppId;
      TProtoConnection  Conn;
      int               hSock;
      int               ReqNum;
}
class AppItemVideo{
public:
      int Codec;
      int Width;
      int Height;
      int Fps;
      int StartTime;
}

class TProtocol{

class AppItem{
      int               Type;         // 1 - video, 2 - audio, 3 - app guid, 4 - stream meta data, 5 initial app data
      string            sGuid;
      int               LinkId;       //для связи несколько потоков в один(например, аудио и видео одной камеры)
      variant           MetaData;
      variant           Obj;
      int               FuncOff;
      int               StreamId;
      int               CustomSize;   //for 5 type (initial app data)
      BYTE              *CustomBuf;
}//class AppItem
class TReqItem{
      BYTE              *pBuf;
      int               Size;
}//TReqItem
class TReq{
private:
      int               iPkt;              //==0 это пакет, а не запрос, требующий ответа, ==1 request, ==2 answer
      int               CurOff;            
      BYTE              *pBuf;             //пока используется только при получении ответа
      int               Size;              //пока используется только при получении ответа
      int               StreamId;
      variant           Items;   
      int               hSock;
      int               AppId;
      TProtoConnection  Conn;
public:
      int               Type;         // 1 - video, 2 - audio, 3 - app data
      int               ReqNum;
      int               Result;
      int               ControlCode;
      TReq              Prev;
      TReq              Next;
}//TReq
private:
      TReq              FirstReq;
      int               LastStreamId;
      int               SingleReqNum;      //для клиента генерируем запрос
public: 
      TSem2             SemP;              //нужно, потому что могут использоваться разные модули и там моожет быть баг
      int               hSemP;
      int               bServer;           //должен указывать клиент это или сервер
      int               HdrSize;           //размер заголовка протокола
      TPServer          Server;
      TPSocket          Socket;
      TCrypto           Crypto;
      TPack             Pack;
      BYTE              *PubCoded;
      BYTE              *KeyBuf;
      BYTE              *IvBuf;
      BYTE              *PubKey;
      BYTE              *PrivKey;
      
      int               bEncrypted;
      variant           Apps;
      int               bMustBeEncrypted;  //for the server, all connections must be encrypted
      TProtoConnection  FirstConn;

      //TProtocol();
      virtual int       RegisterApp(string ptr sGuid, int ptr hApp, variant ptr Obj, string ptr FuncName);
      virtual int       RegisterVideoStreamByStream(string ptr sGuid, int ptr hApp, int ptr StreamId, int ptr codec, int ptr width, int ptr height, int ptr fps);
      virtual int       RegisterVideoStream(string ptr sGuid, int ptr hApp, int ptr codec, int ptr width, int ptr height, int ptr fps);
      virtual int       AddPubKey(string ptr PubKeyPath);
      virtual int       AddPrvKey(string ptr PrivKeyPath);
      virtual int       GenerateDescription(TCrypto ptr CurCrypto, BYTE** RetBuf, int ptr Size, int ptr bEnc);
      virtual int       Connect(string ptr Adr);
      virtual void      OnSocketClose(int ptr hSock, variant ptr SocketData, int ptr ExtType);
      virtual int       DecryptReq(TCrypto ptr CurCrypto, BYTE *InBuf, int ptr InSize, BYTE** OutBuf, int ptr OutSize);
      virtual void      ProcessForClient(variant ptr Var);
      virtual int       RunClient(int ptr hApp);
      virtual int       StartPacketLL(TReq ptr hPacket, int ptr ReqNumW);
      virtual int       StartPacket(int ptr hAppId, int ptr StreamId, int ptr PacketType, TReq ptr hPacket);
      virtual int       StartPacketAnswer(int ptr SocketId, int ptr AppId, int ptr StreamId, TReq ptr hPacket);
      virtual int       StartAppAnswer(int ptr Result, TReq ptr InReq, TReq ptr hAnswerReq);
      virtual int       StartAppRequest(int ptr hAppId, int ptr ControlCode, TReq ptr hReq);
      virtual int       Send(TReq ptr Req);
      virtual void      Delete(TReq ptr hReq);
      virtual int       ProcessDescription(int ptr hSock, DWORD *pBuf, int ptr Size, TProtoConnection ptr Proto);
      virtual int       OnAccept(TProtoConnection ptr Conn, int ptr hSock, DWORD *dwBuf, DWORD *pBuf, int ptr Size);   
      virtual int       AcceptConnection(int ptr hSock);
      virtual int       AddString(TReq ptr hPacket, string ptr Str);
      virtual int       GetString(TReq ptr hPacket, string ptr Str);
      virtual int       GetBuf(TReq ptr hPacket, BYTE **Buf, int ptr SizeW);
      virtual int       AddBuf(TReq ptr hPacket, BYTE *Buf, int ptr SizeW, int ptr bCopy);
      virtual int       PrepareEncryptedPackage(TCrypto ptr CurCrypto, BYTE *SrcBuf, int SrcSize, BYTE** OutBufR, int ptr OutSize);
      virtual int       AddVariant(TReq ptr hPacket, variant ptr Variant);
      virtual int       GetVariant(TReq ptr hPacket, variant ptr Variant);
      virtual void      CopySockData(TReq ptr hReq, TProtoSockData ptr SockData);
}
class TProtoCItem{
      TProtocol  Protocol;
      int        hApp;
}
